# Python3 Solutions to *Cracking the Coding Interview, 6th Edition*

My solutions in Python3 for the book
[Cracking the Coding Interview, 6th Edition](https://www.crackingthecodinginterview.com/)
by *Gayle Laakmann McDowell*.

The ``official'' solutions are
[here](https://github.com/careercup/CtCI-6th-Edition-Python) on Github.

They are more complete in the sense that they often show both the most
"pythonic" versions as well as the versions hinted at in the book, which in my
opinion sometimes teach bad habits, like e.g. assuming that charset is ASCII or
mutating strings in place, as seen in Chapter 1.

Non-English Languages Matter!, you know.

On the other hand, it is indeed good to know the difference between unicode
strings and ASCII strings.

## How to use?

The test cases are included in the solution files.

I run them this way: `python3 -m pytest -vv 1_1.py`. For that
you'll need to have the ``pytest'' module installed.

