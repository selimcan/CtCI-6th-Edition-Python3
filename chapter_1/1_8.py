## Best conceivable complexity:
## - time: Θ(M*N)
## - space: Θ(1)

import copy

## A Matrix is a (Listof (Listof Number))
## interp. self-explanatory

MATRIX_1 = [[0, 2, 3, 4 ],
            [4, 5, 0, 23],
            [7, 8, 9, 12]]


def zero_matrix(mat):
    """ Matrix -> Matrix

    If an element in an m x n matrix is 0, set its entire row and column to 0.
    """
    res = copy.deepcopy(mat)
    m = len(mat)
    n = len(mat[0])
    zero_in_row = [False] * m
    zero_in_col = [False] * n
    for i in range(m):
        for j in range(n):
            if mat[i][j] == 0:
                zero_in_row[i] = True
                zero_in_col[j] = True
    for i in range(m):
        for j in range(n):
            if zero_in_row[i] or zero_in_col[j]:
                res[i][j] = 0
    return res

def test_zero_matrix():
    assert zero_matrix(MATRIX_1) == [[0, 0, 0, 0],
                                     [0, 0, 0, 0],
                                     [0, 8, 0, 12]]

## Analysis:
## - time: Θ(m*n)
## - space: Θ(m*n)
##
## If we'd modify the given matrix in place, the space complexity would be Θ(m+n)
