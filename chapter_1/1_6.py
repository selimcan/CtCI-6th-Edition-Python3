## Best conceivable complexity:
## - time: Θ(s)
## - space: Θ(s)


## String -> String
def compress(s):
    """Compress the given string using the counts of repeated characters.
    If the compressed string would not be smaller than the original
    string, return the original string.

    ASSUME: the given string has only uppercase and lowercase letters (a-z).
    """
    if len(s) <= 2:
        return s
    else:
        compressed = []
        count = 0
        for i in range(len(s)):
            count += 1
            if i + 1 >= len(s) or s[i] != s[i + 1]:
                compressed.append(s[i])
                compressed.append(str(count))
                count = 0
        return ''.join(compressed) if len(compressed) < len(s) else s

def test_compress():
    assert compress('') == ''
    assert compress('a') == 'a'
    assert compress('aaa') == 'a3'
    assert compress('abcd') == 'abcd'
    assert compress('aabcccccaaa') == 'a2b1c5a3'

    
## Analysis:
## - time: Θ(s)
## - space: Θ(s)
##
## Repeated string concatenation in Python doesn't seem to suffer from the
## `copying the same thing over and over' problem mentioned in the book,
## which means that using a list, append and join probably is not necessary
## and `compressed += s[i]' etc. would've had the same runtime. Not too sure.
