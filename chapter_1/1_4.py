from collections import Counter


def is_pp(s: str) -> bool:
    """Produce True if `s' is a permutation of a palindrome (i.e. a palindrome
    can be constructed out of it. Ignore whitespace. Ignore case.
    """
    counts = Counter()
    for char in s.lower():
        if char != ' ':
            counts[char] += 1
    # observation: at most one char in a palindrome-able string can
    # have an odd number of occurrences (the one in the middle)
    odds = 0
    for char, count in counts.items():
        if count % 2 == 1:
            odds += 1
            if odds > 1:
                return False
    return True


def test_is_pp():
    assert is_pp('Tact Coa') is True  # 'taco cat' 'atco cta' etc.
    assert is_pp('apple') is False
    assert is_pp('abba') is True
    assert is_pp('ab ba ab ba') is True


###########
## Analysis
##
## Complexity of is_pp:
##   - time: Θ(s), where s is the length of the given string;
##   - space: Θ(c) <= Θ(s), where c is the size of the charset we're
##            dealing with.
