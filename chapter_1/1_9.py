## Best conceivable complexity? No warranty here...
## - time: Θ(s1 + s2)
## - space: Θ(1) ?


## GIVEN


## String String -> Boolean
def is_substring(s1, s2):
    """Return True if s1 is substring of s2, False otherwise."""
    return s1 in s2


## SOLUTION


## String String -> Boolean
def is_rotation(s1, s2):
    """Return True if s2 is a rotation of s1, False otherwise."""
    prefix = longest_prefix(s1, s2)
    suffix = s1[len(prefix):]
    return is_substring(suffix + prefix, s2)

def test_is_rotation():
    assert is_rotation('erbottlewat', 'waterbottle') == True
    assert is_rotation('erwatbottle', 'waterbottle') == False


## String String -> String
def longest_prefix(s1, s2):  ## Θ(n^2)
    """Return longest prefix of s1 which is in s2."""
    split, i = 0, 0
    for j in range(len(s2)):
        while j < len(s2):
            if s1[i] == s2[j]:
                i += 1
                j += 1
            else:
                if i > split:
                    split = i
                i = 0
                break
    return s1[:split]

def test_longest_prefix():
    assert longest_prefix('ab', '') == ''
    assert longest_prefix('erbottlewat', 'waterbottle') == 'erbottle'


## ANALYSIS:
##
## longest_prefix's complexity is Θ(s2^2), thus is_rotation's complexity =
## Θ(s2^2) + is_substring's complexity (which probably is also n^2)... Not good?
