## Best conceivable complexity:
## - time: Θ(n^2)
## - space: Θ(n^2)


###################
## Data Definitions


## SquareMatrix is Listof(Listof(Object)),
## where the length of the outer list = the length of the inner list >= 2
## interp. a square matrix, represented by a list of its rows.
##
SM1 = [[1,   2,  3,  4],
       [5,   6,  7,  8],
       [9,  10, 11, 12],
       [13, 14, 15, 16]]

## Image is a SquareMatrix, where Objects in inner lists represent pixels.


############
## Functions


## SquareMatrix -> SquareMatrix
def rotate(sm):
    """Rotate the sqaure matrix by 90 degrees clockwise."""
    return reflect(reverse_rows(sm))

def test_rotate():
    assert rotate(SM1) == [[13,  9,  5,  1],
                           [14, 10,  6,  2],
                           [15, 11,  7,  3],
                           [16, 12,  8,  4]]


## SquareMatrix -> SquareMatrix
def reverse_rows(sm):
    """Reverse the rows (reflect them across the vertical axis) of the matrix."""
    return [row[::-1] for row in sm]

def test_reverse_rows():
    assert reverse_rows(SM1) == [[4,   3,  2,  1],
                                 [8,   7,  6,  5],
                                 [12, 11, 10,  9],
                                 [16, 15, 14, 13]]


## SquareMatrix -> SquareMatrix
def reflect(sm):
    """Reflect the matrix across the y=x diagonal. Mutates the matrix given."""
    row, col = len(sm) - 1, 0  ## (indicies of) the value on the diaginal
    while row != 0 and col != len(sm):   ## [[        x,  x, x],
        t_row = row                      ##  [        x,  x, x],
        b_col = col                      ##  [(row, col), x, x]] 
        while t_row > 0:             
            t_row -= 1                   ## [[           x,            x, x], 
            b_col += 1                   ##  [(t_row, col),            x, x],
            temp = sm[t_row][col]        ##  [  (row, col), (row, b_col), x]]
            sm[t_row][col] = sm[row][b_col]
            sm[row][b_col] = temp
        row -= 1
        col += 1
    return sm

def test_reflect_across_diag():
    assert reflect([[1, 2, 3],
                    [3, 4, 5],
                    [6, 7, 8]]) == [[8, 5, 3],
                                    [7, 4, 2],
                                    [6, 3, 1]]


## ANALYSIS OF THE RESULT:
## - time: Θ(n^2)
## - space: Θ(n^2)
##
## All of the functions above could change the matrix in place (`reflect' does),
## but probably shouldn't.
##
## The soultion given in the book is of the same complexity, though seems to be
## much more elegant.
