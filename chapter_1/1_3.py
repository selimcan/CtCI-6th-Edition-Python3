def urlify(s: str, l: int) -> str:  #
    """Given a string and its ``true'' length (i.e. the length of the prefix
    up to the trailing whitespace symbols, if any), return a new string where
    spaces were replaced by '%20'.
    """

    def urlify_v1(s, l): # Θ(l) runtime, Θ(l) space
        return ''.join('%20' if c == ' ' else c for c in s[:l])

    return urlify_v1(s, l)


def test_urlify():
    assert urlify('Mr John Smith    ', 13) == 'Mr%20John%20Smith'
    assert urlify('Mr   John Smith', 15) == 'Mr%20%20%20John%20Smith'
