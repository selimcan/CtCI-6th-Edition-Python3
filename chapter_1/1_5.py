## Complexity it is reasonable to expect (aka `Best conceivable runtime' (BCR)
## in the book. `Best conceivable complexity' would also be a good name imho):
## - time: Θ(s1 + s2), where s1 and s2 are the lengths of the strings,
## - space: Θ(1).


## VERSION 1


## String String -> Boolean
def one_away_v1(s1, s2):
    """Given two strings, check if they are at most one edit away from each
    other, where `one edit' is one of the following:
    - insert a character
    - delete a character
    - replace a character.

    Ignore case.
    """
    return lev_dist(s1, s2) <= 1

def test_one_away_v1():
    assert one_away_v1('pale', 'ple') == True
    assert one_away_v1('pales', 'pale') == True
    assert one_away_v1('pale', 'bale') == True
    assert one_away_v1('pale', 'bake') == False
    assert one_away_v1('pale', 'PaLe') == True


## String String -> Integer
def lev_dist(s1, s2):
    """Calculate Levenstein distance between s1 and s2. Each operation (inser-
    tion, deletion, replacement) has weight 1. Case-insensitive.
    """
    memo = dict()
    s1, s2 = s1.lower(), s2.lower()
    if len(s1) == 0:
        return len(s2)
    elif len(s2) == 0:
        return len(s1)
    elif len(s1) == 1 and len(s2) == 1:
        if s1 == s2:
            return 0
        else:
            return 1
    elif (s1, s2) in memo:
        return memo[(s1, s2)]
    else:
        memo[(s1, s2)] = min(lev_dist(s1[:-1],
                                      s2[:-1]) + lev_dist(s1[-1], s2[-1]),
			     lev_dist(s1[:-1], s2) + 1,
		             lev_dist(s1, s2[:-1]) + 1)
        return memo[(s1, s2)]

def test_lev_dist():
    assert lev_dist("", "") == 0
    assert lev_dist("a", "") == 1
    assert lev_dist("", "a") == 1
    assert lev_dist("a", "a") == 0
    assert lev_dist("a", "b") == 1
    assert lev_dist("ab", "cd") == 2
    assert lev_dist("pale", "ple") == 1
    assert lev_dist("pales", "pale") == 1
    assert lev_dist("pale", "bale") == 1
    assert lev_dist("pale", "bake") == 2


## ANALYSIS OF VERSION 1
##
## As far as I can tell, lev_dist runs in Θ(s1 * s2) time and takes Θ(s1 * s2)
## space. This is higher than the complexity I'd thought was achievable in the
## beginning. I simply happened to remember the Levenstein distance and thus
## started by implementing it to solve this problem. The mistake I made though
## was that I stopped right after and, for my disappointment for having done so
## too hastily, jumped to the to the solution given at the end of the book
## (Versions 2 and 3 below).
##
## LESSON: analyse the solution found carefully, especially if its comlexity is
## higher than initially expected. There might be ways to optimize.


## VERSION 2


## String String -> Boolean
def one_away_v2(s1, s2):
    """Given two strings, check if they are one edit away from each other, where
    `one edit' is one of the following:
    - insert a character
    - delete a character
    - replace a character.

    Ignore case.
    """
    s1, s2 = s1.lower(), s2.lower()
    if len(s1) == len(s2):
        return one_replace(s1, s2)
    elif len(s1) + 1 == len(s2):
        return one_insert(s1, s2)
    elif len(s1) - 1 == len(s2):
        return one_insert(s2, s1)
    else:
        return False

def test_one_away_v2():
    assert one_away_v2('pale', 'ple') == True
    assert one_away_v2('pales', 'pale') == True
    assert one_away_v2('pale', 'bale') == True
    assert one_away_v2('pale', 'bake') == False
    assert one_away_v2('pale', 'PaLe') == True


## String String -> Boolean
def one_replace(s1, s2):
    """Given two strings (of identical length), return True if they are identical
    to each other except for only one letter, False otherwise.
    """
    found_difference = False
    for i in range(len(s1)):
        if s1[i] != s2[i]:
            if found_difference:
                return False
            found_difference = True
    return True

def test_one_replace():
    assert one_replace('pale', 'ple') == False
    assert one_replace('pale', 'bale') == True
    assert one_replace('pale', 'bake') == False


## String String -> Boolean
def one_insert(s1, s2):
    """Given two strings (where s1 is exactly one character longer than s2),
    check if it's possible to insert a character into s1 to make s2.
    """
    index1 = 0
    index2 = 0
    while index1 < len(s1) and index1 < len(s1):
        if s1[index1] != s2[index2]:
            if index1 != index2:
                return False
            index2 += 1
        else:
            index1 += 1
            index2 += 1
    return True

def test_one_insert():
    assert one_insert('pale', 'ple') == False
    assert one_insert('ple', 'pale') == True

    
## VERSION 3


## String String -> Boolean
def one_away_v3(first, second):
    """Given two strings, check if they are one edit away from each other, where
    `one edit' is one of the following:
    - insert a character
    - delete a character
    - replace a character.

    Ignore case.
    """

    ## Length cheks
    if abs(len(first) - len(second)) > 1:
        return False

    ## Get shorter and longer string
    s1 = first.lower() if len(first) < len(second) else second.lower()
    s2 = second.lower() if len(first) < len(second) else first.lower()

    index1 = 0
    index2 = 0
    found_difference = False
    while index1 < len(s1) and index2 < len(s2):
        if s1[index1] != s2[index2]:
            ## Ensure that this is the first difference found.
            if found_difference:
                return False
            found_difference = True

            if len(s1) == len(s2):  ## On replace, move shorter pointer
                index1 += 1
        else:  ## If matching, move shorter pointer
            index1 += 1
        index2 += 1  ## Always move pointer for longer string
    return True

def test_one_away_v3():
    assert one_away_v3('pale', 'ple') == True
    assert one_away_v3('pales', 'pale') == True
    assert one_away_v3('pale', 'bale') == True
    assert one_away_v3('pale', 'bake') == False
    assert one_away_v3('pale', 'PaLe') == True
