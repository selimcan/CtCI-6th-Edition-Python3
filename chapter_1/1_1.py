def is_unique(s: str) -> bool:
    """Produce True if `s` has all unique characters, False otherwise."""

    def is_unique_v0(s):  # Θ(s) runtime, Θ(s) space
        return len(s) == len(set(s))

    def is_unique_v1(s):  # Θ(s) runtime, Θ(s) space
        seen = set()
        for c in s:
            if c in seen:
                return False
            else:
                seen.add(c)
        return True

    # ``What if you cannot use additional data structures?''
    def is_unique_v2(s):  # Θ(s*log(s) + s) runtime, Θ(s) space
        """ASSUME: `s` is sorted."""
        if len(s) < 2:
            return True
        elif s[0] == s[1]:
            return False
        else:
            return is_unique_v2(s[2:])

    def is_unique_v3(s):  # Θ(s*log(s) + s) runtime, Θ(s) space
        prev_char = None
        for c in sorted(s):
            if c == prev_char:
                return False
            else:
                prev_char = c
        return True

    return is_unique_v0(s)
    # return is_unique_v1(s)
    # return is_unique_v2(sorted(s))
    # return is_unique_v3(s)


def test_is_unique():
    assert is_unique("") is True
    assert is_unique("a") is True
    assert is_unique("ab") is True
    assert is_unique("aa") is False
    assert is_unique("aba") is False
