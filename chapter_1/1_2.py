from collections import Counter


def permuted(s1: str, s2: str) -> bool:
    """Given two strings, produce True if one is a permutation of the other,
    False otherwise. Case and whitespace are significant.
    """
    if len(s1) != len(s2):
        return False
    else:
        return Counter(s1) == Counter(s2)


def test_permuted():
    assert permuted("", "") is True
    assert permuted("a", "a") is True
    assert permuted("aa", "aa") is True
    assert permuted("aba", "baa") is True
    assert permuted("aba", "cba") is False
    assert permuted("rumba", "aubmr") is True
