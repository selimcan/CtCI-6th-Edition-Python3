
from linked_list import Node, LL_0, LL_1, LL_2, LL_3, LL_4, LL_5, LL_6


####################################################################
## Best conceivable complexity / Complexity that I expect to achieve

## - time: Θ(n)
## - space: Θ(1)


############
## Functions


def partition(ll, x):
    """ (LinkedList) Object -> LinkedList
    
    Partition ll around value x such that all nodes less than x come before all
    nodes greater than or equal to x. Otherwise order of elements in the result
    doesn't matter and preserving the original relative order is not guaranteed.
    """
    def list2linkedlist(l):
        if len(l) == 0:
            return None
        elif len(l) == 1:
            return Node(l[0], None)
        else:
            return Node(l[0], list2linkedlist(l[1:]))

    if not ll or not ll.next:
        return ll
    cur, left, right = ll, [], []
    while cur:
        if cur.val < x:
            left.append(cur.val)
        else:
            right.append(cur.val)
        cur = cur.next
    return list2linkedlist(left + right)

def test_partition():
    assert partition(None, 2) == None
    assert partition(Node(1, None), 2) == Node(1, None)
    assert partition(Node(3, Node(1, None)), 2) == Node(1, Node(3, None))
    assert partition(Node(3,
                          Node(5,
                               Node(8,
                                    Node(5,
                                         Node(10,
                                              Node(2,
                                                   Node(1, None))))))), 5) ==\
          Node(3,
               Node(2,
                    Node(1,
                         Node(5,
                              Node(8,
                                   Node(5,
                                        Node(10, None)))))))


###########
## Analysis


## partition:
##   - time comlexity: Θ(n)
##   - space complexity: Θ(n)
##
## The exact behavior is different from the one described in the example in the
## book, but it meets the specification nonetheless.
##
## A straightforward solution, but with Θ(n) space complexity. I think that
## instead of intermediately storing observed elements in two arrays, we could
## make multiple passes through the original linked list and build the result
## linked list on the fly, but that would still mean Θ(n) space complexity.
