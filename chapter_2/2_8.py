from linked_list import Node, LinkedList


####################################################################
## Best conceivable complexity / Complexity that I expect to achieve

## - time: Θ(n)
## - space: Θ(1)

def detect_loop(ll: LinkedList) -> Node:
    Node("C", None)

def test_detect_loop():
    pass

## hmm, does our implementation of linked lists render such isa loops impossible?
