

from linked_list import Node


####################################################################
## Best conceivable complexity / Complexity that I expect to achieve

## - time: Θ(ll1 + ll2)
## - space: Θ(1)


############
## Functions


def my_sum(ll1, ll2):
    """ (LinkedList LinkedList) -> LinkedList
    
    Sum two numbers ll1 and ll2, represented as linked lists.

    ASSUME: the digits are stored in reverse order, i.e. the head of the linked
            list is the *rightmost* digit of the number which that linked list
            represents.
    """
    if not ll1:
        return ll2
    if not ll2:
        return ll1
    rem = 0
    res = cur = Node(None, None)
    while ll1 and ll2:
        s = ll1.val + ll2.val + rem
        d = s % 10
        rem = 1 if s > 9 else 0
        cur.next = Node(d, None)
        cur, ll1, ll2 = cur.next, ll1.next, ll2.next
    if ll1:
        cur.next = my_sum(Node(rem, None), ll1)
    elif ll2:
        cur.next = my_sum(Node(rem, None), ll2)
    elif rem == 1:
        cur.next = Node(rem, None)
    return res.next

def test_partition():
    assert my_sum(Node(7,                                 ## 617 + 295 = 912
                       Node(1,
                            Node(6, None))),
                  Node(5,
                       Node(9,
                            Node(2, None)))) == Node(2, Node(1, Node(9, None)))
    assert my_sum(Node(1, None), None) == Node(1, None)  ## 1 + None
    assert my_sum(None, Node(2, None)) == Node(2, None)   ## None + 2    
    assert my_sum(Node(0, Node(4, None)),  ## 40 + 3 = 43
                  Node(3, None)) ==  Node(3, Node(4, None))
    assert my_sum(Node(9,                  ## 879 + 586 = 1465
                       Node(7,
                            Node(8, None))),
                  Node(6,
                       Node(8,
                            Node(5, None)))) == Node(5,
                                                     Node(6,
                                                          Node(4,
                                                               Node(1, None))))


###########
## Analysis


## sum(ll1, ll2):
##   - time comlexity: Θ(ll1 + ll2)
##   - space complexity: Θ(1)

