
from linked_list import Node, LL_0, LL_1, LL_2, LL_3, LL_4, LL_5, LL_6


####################################################################
## Best conceivable complexity / Complexity that I expect to achieve

## - time: Θ(1)
## - space: Θ(1)


############
## Functions


def delete_middle_node(ll):
    """(LinkedList) -> Boolean

    Delete a node in the middle (i.e. any node but the last node, not
    necessarily the exact middle) of a *singly*-linked list, given only access
    to that node. Return True if successfull, False otherwise (i.e. if the node
    given turns out to be the last one in the list).
    """
    if not ll or not ll.next:
        return False
    ll.val = ll.next.val
    ll.next = ll.next.next
    return True

def test_delete_middle_node():
    ll = Node('a',
              Node('b',
                   Node('c',
                        Node('d',
                             Node('e',
                                  Node('f', None))))))
    delete_middle_node(ll.next.next)
    assert ll == Node('a',
                      Node('b',
                           Node('d',
                                Node('e',
                                     Node('f', None)))))


###########
## Analysis


## delete_middle_node:
##   - time complexity: Θ(1)
##   - space complexity: Θ(1)
