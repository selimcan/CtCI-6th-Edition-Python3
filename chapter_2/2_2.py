
from collections import deque
from linked_list import Node, LL_0, LL_1, LL_2, LL_3, LL_4, LL_5, LL_6


####################################################################
## Best conceivable complexity / Complexity that I expect to achieve

## - time: Θ(n)
## - space: Θ(k)


############
## Functions


## LinkedList Integer -> Object
def kth_to_last_v1(ll, k):
    """Return the kth-to-last element from a singly-linked list.
    ASSUME: The last element = 0th to last.
    """

    def linked_list_len(ll):
        if not ll:
            return 0
        elif not ll.next:
            return 1
        else:
            return 1 + linked_list_len(ll.next)

    def kth(ll, k):
        if not ll or k < 0:
            return None
        elif k == 0:
            return ll.val
        else:
            return kth(ll.next, k-1)
        
    if not ll:
        return None
    else:
        n = linked_list_len(ll)
        return kth(ll, n - k - 1)
    
def test_kth_to_last_v1():
    assert kth_to_last_v1(LL_0, 0) == None
    assert kth_to_last_v1(LL_0, 4) == None
    assert kth_to_last_v1(LL_2, 0) == "banana"
    assert kth_to_last_v1(LL_2, 1) == "apple"

    
## LinkedList Integer -> Object
def kth_to_last_v2(ll, k):
    """Return the kth-to-last element from a singly-linked list."""
    if not ll:
        return None
    else:
        cur = ll
        mem = deque([cur.val], k + 1)  ## up to k + 1 last elements will be kept
        while cur.next != None:
            cur = cur.next
            mem.append(cur.val)
    return kth_to_last_from_deque(mem, k)

def test_kth_to_last_v2():
    assert kth_to_last_v2(LL_0, 0) == None
    assert kth_to_last_v2(LL_0, 4) == None
    assert kth_to_last_v2(LL_2, 0) == "banana"
    assert kth_to_last_v2(LL_2, 1) == "apple"


## Deque Integer -> Object
def kth_to_last_from_deque(d, k):
    """Return the kth-to-last element from d."""
    if len(d) < k + 1:
        return None
    else:
        return d[-1 - k]

def test_kth_to_last_from_deque():
    D = deque(['a', 'b', 'c', 'd'])
    assert kth_to_last_from_deque(deque(), 2) == None
    assert kth_to_last_from_deque(D, 0) == 'd'
    assert kth_to_last_from_deque(D, 2) == 'b'
    assert kth_to_last_from_deque(D, 6) == None


###########
## Analysis

## kth_to_last_v1 is a recursive solution.
## Time complexity of kth_to_last_v1: Θ(n)
## Space complexity of kth_to_last_v1: Θ(n) (would've been Θ(1) if there were
## tail call optimization in Python, I guess.
##
## Time complexity of kth_to_last_v2: Θ(n)
## Space complexity of kth_to_last_v2: Θ(k).
## The idea was that if we want to get kth to last element from a singly-linked
## list, than it's enough to keep *the last* k+1 elements seen so far.
##
## TODO kth_to_last_v3 develops this idea further, and stores only two elements
## at all times: the last one, and kth to last one.

