
from typing import Union


###################
## Data definitions


class Node:    

    def __init__(self, val, next):
        self.val = val
        self.next = next

    def __eq__(self, other):
        if not other:
            return False
        else:
            return self.val == other.val and self.next == other.next

    def __repr__(self):
        return "Node('" + str(self.val) + "', " + str(self.next) + ')'

    def __iter__(self):
        cur = self
        while cur:
            yield cur
            cur = cur.next
            
## Node is Node(Object, LinkedList)
## interp. a node in a singly-lingked list with its value and the next node.

## fn_for_node(n):
##     ... n.val                       ## Object
##     ... fn_for_linked_list(n.next)


LinkedList = Union[Node, None]

LL_0 = None
LL_1 = Node(31, None)                       ## 31
LL_2 = Node('apple', Node('banana', None))  ## apple -> banana
LL_3 = Node(1, Node(1, None))
LL_4 = Node(1, None)
LL_5 = Node(1, Node(2, Node(1, Node(2, None))))
LL_6 = Node(1, Node(2, None))

## fn_for_linked_list(ll):
##     if not ll:
##         ...
##     else:
##         fn_for_node(ll)
