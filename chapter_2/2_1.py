
from linked_list import Node, LL_0, LL_1, LL_2, LL_3, LL_4, LL_5, LL_6


##################################################
## Best conceivable complexity/complexity expected


## Θ(n) time
## Θ(n) space


############
## Functions


## LinkedList -> LinkedList
def remove_dups(ll):
    """Remove duplicated nodes from an unsorted linked list."""

    def _remove_dups(ll):
        if not ll:
            return None
        elif ll.val in seen:
            return _remove_dups(ll.next)
        else:
            seen.add(ll.val)
            return Node(ll.val, _remove_dups(ll.next))

    seen = set()
    return _remove_dups(ll)

def test_remove_dups():
    assert remove_dups(None) == None
    assert remove_dups(LL_1) == LL_1
    assert remove_dups(LL_2) == LL_2
    assert remove_dups(LL_3) == LL_4
    assert remove_dups(LL_5) == LL_6


## LinkedList -> LinkedList
def remove_dups_v2(ll):
    """Remove duplicated nodes from an unsorted linked list."""
    if not ll:
        return None
    current = ll
    while current:
        runner = current
        while runner.next:
            if runner.next.val == current.val:
                runner.next = runner.next.next
            else:
                runner = runner.next
        current = current.next
    return ll

def test_remove_dups_v2():
    assert remove_dups_v2(None) == None
    assert remove_dups_v2(LL_1) == LL_1
    assert remove_dups_v2(LL_2) == LL_2
    assert remove_dups_v2(LL_3) == LL_4
    assert remove_dups_v2(LL_5) == LL_6


###########
## Analysis


## remove_dups' complexity is Θ(n) time and Θ(n) space.
## 
## remove_dups_v2 is a solution for the follow-up question. Complexity there
## seems to be Θ(n^2) time and Θ(1) space.
