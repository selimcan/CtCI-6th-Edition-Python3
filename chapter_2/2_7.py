
from linked_list import Node, LinkedList

def are_intersecting(ll1: LinkedList, ll2: LinkedList) -> bool:
    """ Return True if `ll1` and `ll2` intersect, False otherwise.

    By intersect we mean that some k-th node of ll1 and j-th of ll2 point, refer
    to the same node.
    """
    for n1 in ll1:
        for n2 in ll2:
            if n1 is n2:
                return True
    return False

def test_are_intersecting():
    n = Node('c', Node('d', Node('e', None)))
    assert are_intersecting(Node('a', Node('b', n)),
                            Node('f', Node('b', Node(2,  Node('h', n))))) == True
    assert are_intersecting(Node(1, Node('c', Node('d', Node('e', None)))),
                            n) == False
